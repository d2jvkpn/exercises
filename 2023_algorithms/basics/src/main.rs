#![allow(dead_code)]

mod math;
mod searching;
mod sorting;
mod structures;

fn main() {
    println!("Hello, world!");
}
