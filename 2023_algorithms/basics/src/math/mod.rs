#![allow(dead_code)]

mod factorial;
mod fibonacci;
mod greatest_common_divisor;
mod is_prime;
mod reverse_int;
