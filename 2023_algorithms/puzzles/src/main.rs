#![allow(dead_code)]

mod islands_in_matrix;
mod palindrome;
mod slice_to_max_num;
mod sort_positive_negative;
mod tower_of_hanoi;

fn main() {
    println!("Hello, world!");
}
